#include <iostream>
#include<math.h>

class Vector
{
    //initialize
private:
    double x;
    double y;
    double z;
public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    //print x y z
    void Show()
    {
        std::cout << '\n' << x << " " << y << " " << z;

    }
    //calcukate vector length
    double Size()
    {
        double size = pow( (x * x + y * y + z * z), 0.5);
        return size;
    }
};

int main()
{
    //user data input
    double x;
    double y;
    double z;
    std::cout << "Input x: ";
    std::cin >> x;
    std::cout <<  "Input y: ";
    std::cin >> y;
    std::cout << "Input z: ";
    std::cin >> z;

    Vector v(x, y, z), v1(x * 2, y * 2, z * 2);
    v.Show();
    v.Size();
    std::cout << "\n" << "Vector Length = " << v.Size();

    //doublle-sized vector
    std::cout << "\n" << "Vector (double-sized)";
    v1.Show();
    v1.Size();
    std::cout << "\n" << "Vector Length = " << v1.Size();

}

